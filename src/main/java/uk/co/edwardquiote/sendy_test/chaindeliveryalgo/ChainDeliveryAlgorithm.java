package uk.co.edwardquiote.sendy_test.chaindeliveryalgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import uk.co.edwardquiote.sendy_test.chaindeliveryalgo.Models.ModelDeliveryOrder;
import uk.co.edwardquiote.sendy_test.chaindeliveryalgo.Models.ModelLocationPoints;

/**
 *
 * @author Edward Ndukui
 */
public class ChainDeliveryAlgorithm {

    public static void main(String[] args) {

        double dIhubLat = -1.2892906;
        double dIhubLong = 36.7809593;
        double dSendyLat = -1.3001043;
        double dSendyLong = 36.7706334;
        double dPrestigeLat = -1.3003656;
        double dPrestigeLong = 36.7848893;
        double dJunctionLat = -1.2981102;
        double dJunctionLong = 36.7608815;
        double dAdamsArcadeLat = -1.3005641;
        double dAdamsArcadeLong = 36.778638;

        double[] daryPickUpPointsLatitudes = {
            dIhubLat, dSendyLat, dSendyLat
        };
        double[] daryPickUpPointsLongitudes = {
            dIhubLong, dSendyLong, dSendyLong
        };
        double[] daryDropOffPointsLatitudes = {
            dPrestigeLat, dJunctionLat, dAdamsArcadeLat
        };
        double[] daryDropOffPointsLongitudes = {
            dPrestigeLong, dJunctionLong, dAdamsArcadeLong
        };

        double[] daryPickUpPoints;
        double[] daryDropOffPoints;
        ArrayList<ModelLocationPoints> arylLocationPoints = new ArrayList<>();
        for (int i = 0; i < daryPickUpPointsLatitudes.length; i++) {
            daryPickUpPoints = new double[]{daryPickUpPointsLatitudes[i], daryPickUpPointsLongitudes[i]};
            daryDropOffPoints = new double[]{daryDropOffPointsLatitudes[i], daryDropOffPointsLongitudes[i]};

            ModelLocationPoints clsLocationPoints = new ModelLocationPoints();
            clsLocationPoints.setDaryPickUpPoints(daryPickUpPoints);
            clsLocationPoints.setDaryDropOffPoints(daryDropOffPoints);

            arylLocationPoints.add(clsLocationPoints);
            System.out.println("Location Points set up: " + arylLocationPoints.size());
        }
        System.out.println("PickUp and Drop-off Points set Up");

        ChainDeliveryAlgorithm clsAlgoClass = new ChainDeliveryAlgorithm();
        ArrayList<ModelDeliveryOrder> arylPlacedDeliveryOrders = clsAlgoClass.codeToCreateNewDeliveryOrders(arylLocationPoints);
        clsAlgoClass.codeToComparePickUpPointsOfDeliveryOrders(arylPlacedDeliveryOrders);
        System.out.println("Placed Delivery Orders set up");
    }

    private ArrayList<ModelDeliveryOrder> codeToCreateNewDeliveryOrders(ArrayList<ModelLocationPoints> arrayListLocationPoints) {

        ArrayList<ModelDeliveryOrder> arylPlacedDeliveryOrders = new ArrayList<>();
        for (int i = 0; i < arrayListLocationPoints.size(); i++) {

            ModelDeliveryOrder clsDeliveryOrder = new ModelDeliveryOrder();
            clsDeliveryOrder.setDaryOrderPickUpPoint(arrayListLocationPoints.get(i).getDaryPickUpPoints());
            clsDeliveryOrder.setDaryOrderDropOffPoint(arrayListLocationPoints.get(i).getDaryDropOffPoints());

            arylPlacedDeliveryOrders.add(clsDeliveryOrder);

            System.out.println("Delivery Order Created and added to List");
        }

        return arylPlacedDeliveryOrders;
    }

    private void codeToComparePickUpPointsOfDeliveryOrders(ArrayList<ModelDeliveryOrder> arrayListPlacedDeliveryOrders) {

        //  Get Pickup Point of A and Compare to B's
        for (int i = 0; i < arrayListPlacedDeliveryOrders.size(); i++) {
            System.out.println("Gets into FOR. . .LOOP to get Pickup Points of A, loop " + (i));
            System.out.println("Size of Placed Delivery Orders List: " + arrayListPlacedDeliveryOrders.size());

            //  Pickup Point of Order A
            double[] daryPickUpPoint = arrayListPlacedDeliveryOrders.get(i).getDaryOrderPickUpPoint();
            System.out.println("PICK UP POINT. Latitude: " + daryPickUpPoint[0] + " Longitude: " + daryPickUpPoint[1]);

            for (int j = (i + 1); j < arrayListPlacedDeliveryOrders.size(); j++) {
                System.out.println("Gets into FOR. . .LOOP to get Pickup Points of B, loop " + (j));

                //  Pickup Point of Order B
                double[] daryNextPickUpPoint = arrayListPlacedDeliveryOrders.get(j).getDaryOrderPickUpPoint();
                System.out.println("NEXT PICK UP POINT. Latitude: " + daryNextPickUpPoint[0] + " Longitude: " + daryNextPickUpPoint[1]);

                //  So are they the same?
                if ((daryPickUpPoint[0] == daryNextPickUpPoint[0]) && (daryPickUpPoint[1] == daryNextPickUpPoint[1])) {     //  YES
                    System.out.println("Orders Pick Up Point MATCH. Latitude: " + daryPickUpPoint[0] + " Longitude: " + daryPickUpPoint[1]);

                    double[] daryDropOffPointA = arrayListPlacedDeliveryOrders.get(i).getDaryOrderDropOffPoint();
                    double[] daryDropOffPointB = arrayListPlacedDeliveryOrders.get(j).getDaryOrderDropOffPoint();
                    if (codeToCompareDropOffPointsOfDeliveryOrdersWithSimilarPickUpPoints(daryDropOffPointA, daryDropOffPointB)) { // Drop-Off Points are Similar.
                        System.out.println("Dropoff Points MATCH. LatitudeA: " + daryDropOffPointA[0] + ", LongitudeA: " + daryDropOffPointA[1]);
                        System.out.println("Dropoff Points MATCH. LatitudeB: " + daryDropOffPointB[0] + ", Longitudeb: " + daryDropOffPointB[1]);

                        //  Here, I can now schedule a single delivery trip.
                        codeToScheduleSingleTripDeliveryOrders(daryPickUpPoint, arrayListPlacedDeliveryOrders.get(i).getDaryOrderDropOffPoint());
                    } else {    //  Drop-off Points aren't Similar

                        //  Now, I calculate distance between the two drop-off points, to decide which on to start with and if a chain-delivery is realistic.
                        int iDistanceBtwnYAndZ = codeToCalculateDistanceDifferenceBetweenDropOffPoints(daryDropOffPointA, daryDropOffPointB);
                        codeToScheduleChainDeliveryOrdersBasedOnDistanceBetweenDropOffPoints(iDistanceBtwnYAndZ, daryPickUpPoint, daryDropOffPointA, daryDropOffPointB);
                    }
                } else {
                    System.out.println("Orders Pick Up Point DO NOT MATCH. Latitude: " + daryPickUpPoint[0] + " Longitude: " + daryPickUpPoint[1]);

                    codeToScheduleIndividualDeliveryOrders(daryPickUpPoint, arrayListPlacedDeliveryOrders.get(i).getDaryOrderDropOffPoint());
                }
            }
        }
    }

    private boolean codeToCompareDropOffPointsOfDeliveryOrdersWithSimilarPickUpPoints(double[] dropOffPointA, double[] dropOffPointB) {
        return (dropOffPointA == dropOffPointB);
    }

    private int codeToCalculateDistanceDifferenceBetweenDropOffPoints(double[] dropOffPointA, double[] dropOffPointB) {
        //  Drop-off point for A = Y            ||          Drop-off Point for B = Z

        int iDistanceBtwnYAndZ = 0;

        try {
            String sEncodingFormat = "UTF-8";
            String sDistanceMatrixResponseUnits = "metric";
            String sDistanceOrigins = dropOffPointA[0] + "," + dropOffPointA[1];
            String sDistanceDestination = dropOffPointB[0] + "," + dropOffPointB[1];
            String sDistanceLanguage = "en-GB";
            String sAPI_KEY = "AIzaSyCHz50_Ed29U5TIvEIsBXJaP8WATi6RBrk";
            String sDistanceMatrixRequestURL = "https://maps.googleapis.com/maps/api/distancematrix/json?units="
                    + URLEncoder.encode(sDistanceMatrixResponseUnits, sEncodingFormat) + "&origins="
                    + URLEncoder.encode(sDistanceOrigins, sEncodingFormat) + "&destinations="
                    + URLEncoder.encode(sDistanceDestination, sEncodingFormat) + "&language="
                    + URLEncoder.encode(sDistanceLanguage, sEncodingFormat) + "&key="
                    + URLEncoder.encode(sAPI_KEY, sEncodingFormat);

            CloseableHttpClient chcHttpClient = HttpClientBuilder.create().build();

            //  Request has to use HTTP_GET Request Method; Can't use POST
            HttpGet htgHttpGet = new HttpGet(sDistanceMatrixRequestURL);
            HttpResponse htrGoogleServerResponse = chcHttpClient.execute(htgHttpGet);
            System.out.println("Response Code: " + htrGoogleServerResponse.getStatusLine().getStatusCode());

            StringBuilder sbResponseContent = new StringBuilder();
            String sReadLine;
            BufferedReader bfrResponseReader = new BufferedReader(new InputStreamReader(htrGoogleServerResponse.getEntity().getContent()));
            while ((sReadLine = bfrResponseReader.readLine()) != null) {
                sbResponseContent.append(sReadLine);
            }
            System.out.println("Response Content: " + sbResponseContent);
            String sResponseContent = sbResponseContent.toString();
            System.out.println("Response Content as String: " + sResponseContent);

            //  Converted my response to String from StringBuilder for easy conversion to JSON format; JSONObject can't convert StringBuilder but can convert String
            JSONObject jsobResponseContent = new JSONObject(sResponseContent);
            
            //  Get First object in our JSON Response: "rows"; Not interested in origin, destination addresses or status for now
            JSONArray jsaryResponseContent_Rows = jsobResponseContent.getJSONArray("rows");
            for (int i = 0; i < jsaryResponseContent_Rows.length(); i++) {
                JSONObject jsobResponseContent_Elements = jsaryResponseContent_Rows.getJSONObject(i);
                JSONArray jsaryResponseContent_Elements = jsobResponseContent_Elements.getJSONArray("elements");

                for (int j = 0; j < jsaryResponseContent_Elements.length(); j++) {

                    JSONObject jsobResponseContent_Distance = jsaryResponseContent_Elements.getJSONObject(j);
                    JSONObject jsobResponseContent_DistanceValue = jsobResponseContent_Distance.getJSONObject("distance");
                    //  The "distance" JSONObject might come at various indexes in the "elements" array, that's the reason of checking for null
                    if (jsobResponseContent_DistanceValue != null) {
                        iDistanceBtwnYAndZ = jsobResponseContent_DistanceValue.getInt("value");

                        System.out.println("Distance is: " + iDistanceBtwnYAndZ + "metres");
                    } else {
                        //  If NULL, then check for "distance" JSONObject in next index without waiting to loop "elements" array again - memory efficieny
                        jsobResponseContent_DistanceValue = jsaryResponseContent_Elements.getJSONObject(j + 1);

                        iDistanceBtwnYAndZ = jsobResponseContent_DistanceValue.getInt("value");

                        System.out.println("Distance is: " + iDistanceBtwnYAndZ + "meters!");
                    }
                }
            }

        } catch (IOException ioex) {
            ioex.printStackTrace();
        }

        return iDistanceBtwnYAndZ;
    }

    private void codeToScheduleIndividualDeliveryOrders(double[] pickUpPoint, double[] dropOffPoint) {
        System.out.println("codeToScheduleIndividualDeliveryOrders() CALLED");
    }

    private void codeToScheduleSingleTripDeliveryOrders(double[] pickUpPoint, double[] dropOffPoint) {
        System.out.println("codeToScheduleSingleTripDeliveryOrders() CALLED");
    }

    private void codeToScheduleChainDeliveryOrdersBasedOnDistanceBetweenDropOffPoints(int distanceBtwnYAndZ, double[] pickUpPoint, double[] dropOffPointA, double[] dropOffPointB) {
        //  Chain Delivery can be set up here now: A queue of deliveries with same pick-up points and different drop-offs

        System.out.println("Chain Delivery Orders Set-up");
    }
}
