package uk.co.edwardquiote.sendy_test.chaindeliveryalgo.Models;

/**
 *
 * @author Edward Ndukui
 */
public class ModelLocationPoints {
    
    private double[] daryPickUpPoints;
    private double[] daryDropOffPoints;

    public double[] getDaryPickUpPoints() {
        return daryPickUpPoints;
    }

    public void setDaryPickUpPoints(double[] daryPickUpPoints) {
        this.daryPickUpPoints = daryPickUpPoints;
    }

    public double[] getDaryDropOffPoints() {
        return daryDropOffPoints;
    }

    public void setDaryDropOffPoints(double[] daryDropOffPoints) {
        this.daryDropOffPoints = daryDropOffPoints;
    }
}
