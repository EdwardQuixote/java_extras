package uk.co.edwardquiote.sendy_test.chaindeliveryalgo.Models;

/**
 *
 * @author Edward Ndukui
 */
public class ModelDeliveryOrder {
    
    private double[] daryOrderPickUpPoint;
    private double[] daryOrderDropOffPoint;
    
    
    public ModelDeliveryOrder() {}
    

    public double[] getDaryOrderPickUpPoint() {
        return daryOrderPickUpPoint;
    }

    public void setDaryOrderPickUpPoint(double[] daryOrderPickUpPoint) {
        this.daryOrderPickUpPoint = daryOrderPickUpPoint;
    }

    public double[] getDaryOrderDropOffPoint() {
        return daryOrderDropOffPoint;
    }

    public void setDaryOrderDropOffPoint(double[] daryOrderDropOffPoint) {
        this.daryOrderDropOffPoint = daryOrderDropOffPoint;
    }
}
